Business Need: We want users to register when credentials are correct. And get proper message when credential are not correct.

Feature: User registration

  Scenario: Successful registration when credentials are correct
    Given Bob has correct credentials prepared
    When Bob makes a request to register endpoint
    Then Registration confirm message is presented
    And Response code of 201 is presented

  Scenario Outline: Check if registration with invalid field will display proper validation error
    Given Bob has invalid "<username>", "<email>", "<pass>" prepared
    When Bob makes a request to register endpoint
    Then Proper validation "<error>" is presented
    Examples:
      | username | email | pass | error |
      | | a@example.com | as8da9sdj98 | \"username\" is not allowed to be empty |
      | adam.jasinski | | as8da9sdj98 | \"email\" is not allowed to be empty |
      | marcin.kowal | a@example.com | | \"pass\" is not allowed to be empty |
      | marcin.kowal | a@@@@example.com | | \"email\" must be a valid email |
      | marcin.kowal | @example.com | | \"email\" must be a valid email |
      | marcin.kowal | a@ | | \"email\" must be a valid email |