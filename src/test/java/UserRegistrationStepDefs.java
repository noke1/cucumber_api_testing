import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import pojo.Message;
import pojo.User;
import pojo.errors.ValidationReason;
import provider.user.UserGenerator;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static provider.user.UserBuilder.anUser;

public class UserRegistrationStepDefs {

    private User user;
    private Response response;

    @Given("Bob has correct credentials prepared")
    public void bobHasCorrectCredentialsPrepared() {
        user = new UserGenerator().generateUser();
        System.out.println(user.toString());
    }

    @When("Bob makes a request to register endpoint")
    public void bobMakesARequestToRegisterEndpoint() {
        response = given()
                .contentType(ContentType.JSON)
                .body(user)
                .post("http://localhost:1337/api/users/register")
                .then()
                .extract()
                .response();
    }

    @Then("Registration confirm message is presented")
    public void registrationConfirmMessageIsPresented() {
        Message message = response.getBody().as(Message.class);
        final String actualMessage = message.getMessage();
        final String userSuccessfullyRegistered = "User successfully registered";

        assertThat(actualMessage)
                .as("Check if response message is the same as expected")
                .isEqualTo(userSuccessfullyRegistered);
    }

    @And("Response code of {int} is presented")
    public void responseCodeOfIsPresented(final int expectedResponseCode) {
        final int actualResponseCode = response.statusCode();

        assertThat(actualResponseCode)
                .as("Actual and response code have to be the same")
                .isEqualTo(expectedResponseCode);
    }

    @Given("Bob has invalid {string}, {string}, {string} prepared")
    public void bobHasInvalidPrepared(final String username, final String email, final String pass) {
        user = anUser()
                .withUsername(username)
                .withEmail(email)
                .withPass(pass)
                .build();
    }

    @Then("Proper validation {string} is presented")
    public void properValidationIsPresented(final String validationReason) {
        final ValidationReason reason = response.getBody().as(ValidationReason.class);
        final String actualReason = reason.getErrors().getReasons().get(0);

        assertThat(actualReason)
                .as("Check if proper message is visible when user data is not correct")
                .isEqualTo(validationReason);
    }
}
