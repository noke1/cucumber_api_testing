package pojo.errors;

import com.fasterxml.jackson.annotation.JsonAlias;

import java.util.List;

public class Errors{

	@JsonAlias({"username","email","pass"})
	private List<String> reasons;

	public void setReasons(List<String> reasons){
		this.reasons = reasons;
	}

	public List<String> getReasons(){
		return reasons;
	}

	@Override
 	public String toString(){
		return 
			"Errors{" + 
			"reasons = '" + reasons + '\'' +
			"}";
		}
}