package pojo.errors;

public class ValidationReason {

	private Errors errors;

	public void setErrors(Errors errors){
		this.errors = errors;
	}

	public Errors getErrors(){
		return errors;
	}

	@Override
 	public String toString(){
		return 
			"Response{" + 
			"errors = '" + errors + '\'' + 
			"}";
		}
}
