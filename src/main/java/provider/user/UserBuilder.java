package provider.user;

import pojo.User;

public final class UserBuilder {
    private String username;
    private String email;
    private String pass;

    private UserBuilder() {
    }

    public static UserBuilder anUser() {
        return new UserBuilder();
    }

    public UserBuilder withUsername(String username) {
        this.username = username;
        return this;
    }

    public UserBuilder withEmail(String email) {
        this.email = email;
        return this;
    }

    public UserBuilder withPass(String pass) {
        this.pass = pass;
        return this;
    }

    public User build() {
        User user = new User();
        user.setUsername(username);
        user.setEmail(email);
        user.setPass(pass);
        return user;
    }
}
