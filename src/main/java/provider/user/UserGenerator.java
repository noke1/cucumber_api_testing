package provider.user;

import com.github.javafaker.Faker;
import pojo.User;

import java.util.Locale;

public class UserGenerator {

    private Faker faker = new Faker(new Locale("pl"));

    public User generateUser() {
        return new User(
            faker.name().username(),
            faker.internet().safeEmailAddress(),
            faker.internet().password()
        );
    }

}
